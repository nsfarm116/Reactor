--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_LFLine = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				Input = InstanceInput {
					SourceOp = "PipeRouter1",
					Source = "Input",
				},
				Controls = InstanceInput {
					SourceOp = "LINE",
					Source = "Controls",
					Default = 0,
				},
				Blank1 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank1",
				},
				BlendClone = InstanceInput {
					SourceOp = "Merge2",
					Source = "BlendClone",
					Default = 1,
				},
				Size = InstanceInput {
					SourceOp = "LINE",
					Source = "Size",
					Default = 1,
				},
				Offset = InstanceInput {
					SourceOp = "Merge1",
					Source = "Offset",
					Default = 1,
				},
				Blank2 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank1",
				},
				GainRed = InstanceInput {
					SourceOp = "ColorGain",
					Source = "GainRed",
					Name = "Line Color",
					ControlGroup = 5,
					Default = 1,
				},
				GainGreen = InstanceInput {
					SourceOp = "ColorGain",
					Source = "GainGreen",
					ControlGroup = 5,
					Default = 1,
				},
				GainBlue = InstanceInput {
					SourceOp = "ColorGain",
					Source = "GainBlue",
					ControlGroup = 5,
					Default = 1,
				},
				GainAlpha = InstanceInput {
					SourceOp = "ColorGain",
					Source = "GainAlpha",
					ControlGroup = 5,
					Default = 1,
				},
				Blank3 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank1",
				},
				Level = InstanceInput {
					SourceOp = "Ellipse",
					Source = "Level",
					Default = 0.8,
				},
				SoftEdge = InstanceInput {
					SourceOp = "Ellipse",
					Source = "SoftEdge",
					Default = 0.2,
				},
				Width = InstanceInput {
					SourceOp = "Ellipse",
					Source = "Width",
					Default = 0.5,
				},
				Height = InstanceInput {
					SourceOp = "Ellipse",
					Source = "Height",
					Default = 0.5,
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "Merge2",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 171.889, 971 },
				Flags = {
					AllowPan = false,
					ForceModes = false,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 663.822, 132.482, 331.911, 24.2424 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 0, 0 }
			},
			Tools = ordered() {
				Merge2 = Merge {
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "PipeRouter1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Clear",
							Source = "Output",
						},
						Gain = Input { Value = 0, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -252.35, 74.8166 } },
				},
				Clear = BrightnessContrast {
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "Ellipse",
							Source = "Mask",
						},
						Alpha = Input { Value = 1, },
						Gain = Input { Value = 0, },
						Input = Input {
							SourceOp = "Merge1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -2.1261, 74.8166 } },
				},
				Merge1 = Merge {
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "PipeRouter1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "ColorGain",
							Source = "Output",
						},
						Center = Input {
							Value = { 0.5, 0.768278791343464 },
							Expression = "(Point(0.5, LINE:GetSourceTool(\"Controls\").PointIn1.Y))*Offset+(Point(0.5, LINE:GetSourceTool(\"Controls\").PointIn2.Y))*(1-Offset)",
						},
						Size = Input { Value = 4, },
						Operator = Input { Value = FuID { "In" }, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -2.1261, 8.69855 } },
					UserControls = ordered() {
						Offset = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ScrewControl",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Offset",
						}
					}
				},
				PipeRouter1 = PipeRouter {
					NameSet = true,
					ViewInfo = PipeRouterInfo { Pos = { -286.627, 8.69855 } },
				},
				ColorGain = ColorGain {
					NameSet = true,
					Inputs = {
						ColorRanges = Input {
							Value = ColorCurves {
								Curves = {
									{
										Points = {
											{ 0, 1 },
											{ 0.125, 0.75 },
											{ 0.375, 0.25 },
											{ 0.5, 0 }
										}
									},
									{
										Points = {
											{ 0.5, 0 },
											{ 0.625, 0.25 },
											{ 0.875, 0.75 },
											{ 1, 1 }
										}
									}
								}
							},
						},
						Input = Input {
							SourceOp = "LINE",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 138.355, 8.69855 } },
					UserControls = ordered() {
						GainRed = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HLS",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 0,
							IC_ControlGroup = 1,
							CLRC_ShowWheel = true,
							LINKS_Name = "Gain Red",
						},
						GainGreen = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HLS",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 1,
							IC_ControlGroup = 1,
							CLRC_ShowWheel = false,
							LINKS_Name = "Gain Green",
						},
						GainBlue = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HLS",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 2,
							IC_ControlGroup = 1,
							CLRC_ShowWheel = true,
							LINKS_Name = "Gain Blue",
						},
						GainAlpha = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HLS",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 3,
							IC_ControlGroup = 1,
							CLRC_ShowWheel = true,
							LINKS_Name = "Gain Alpha",
						}
					}
				},
				Ellipse = EllipseMask {
					NameSet = true,
					Inputs = {
						Level = Input { Value = 0.8, },
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						SoftEdge = Input { Value = 0.2, },
						PaintMode = Input { Value = FuID { "Multiply" }, },
						Invert = Input { Value = 1, },
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Center = Input {
							Value = { 0.795833333333333, 0.607407407407407 },
							Expression = "LINE:GetSourceTool(\"Controls\").PointIn1",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 152.364, 74.8166 } },
				},
				LINE = Background {
					NameSet = true,
					Inputs = {
						Width = Input {
							Value = 480,
							Expression = "PipeRouter1.Input.OriginalWidth/4",
						},
						Height = Input {
							Value = 5,
							Expression = "(Width/96)*Size",
						},
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Type = Input { Value = FuID { "Gradient" }, },
						GradientType = Input { Value = FuID { "Reflect" }, },
						Start = Input { Value = { 0.5, 1 }, },
						End = Input { Value = { 0.5, 0 }, },
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 0, 0, 0, 1 },
									[0.5] = { 1, 1, 1, 1 },
									[1] = { 0, 0, 0, 1 }
								}
							},
						},
						GradientInterpolationMethod = Input { Value = FuID { "LAB" }, },
					},
					ViewInfo = OperatorInfo { Pos = { 268.805, 8.69855 } },
					UserControls = ordered() {
						Controls = {
							LINKID_DataType = "Image",
							LINKS_Name = "Controls",
							IC_ControlPage = 0,
							INPID_InputControl = "ImageControl",
							INP_Default = 0,
						},
						Size = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 10,
							INP_Default = 1,
							INP_MinScale = 1,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Size",
						}
					}
				}
			},
		}
	},
	ActiveTool = "ml_LFLine"
}