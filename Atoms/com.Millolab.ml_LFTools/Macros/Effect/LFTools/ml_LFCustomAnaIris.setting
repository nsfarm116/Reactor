--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_LFCustomAnaIris = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				Input = InstanceInput {
					SourceOp = "Merge4",
					Source = "Background",
				},
				IrisIn = InstanceInput {
					SourceOp = "C_In",
					Source = "Foreground",
				},
				Controls = InstanceInput {
					SourceOp = "IRIS",
					Source = "Controls",
					Default = 0,
				},
				Blank9 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				Start = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "WriteOnStart",
					ControlGroup = 15,
					Default = 0,
				},
				End = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "WriteOnEnd",
					ControlGroup = 15,
					Default = 1,
				},
				ClonesCount = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "ClonesCount",
					Name = "Iris Copies",
					Default = 25,
				},
				Blank1 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				BlendElement = InstanceInput {
					SourceOp = "C_In",
					Source = "Blend",
					Name = "Element Strength",
					Default = 0.5,
				},
				LockXY = InstanceInput {
					SourceOp = "Scale1",
					Source = "LockXY",
					Default = 1,
				},
				SizeMultiplier = InstanceInput {
					SourceOp = "Scale1",
					Source = "XSize",
					Default = 1,
				},
				YSizeMultiplier = InstanceInput {
					SourceOp = "Scale1",
					Source = "YSize",
					Default = 1,
				},
				AngleElement = InstanceInput {
					SourceOp = "C_In",
					Source = "Angle",
					Default = 0,
				},
				LockXYBlur = InstanceInput {
					SourceOp = "Blur1",
					Source = "LockXY",
					Default = 0,
				},
				BlurX = InstanceInput {
					SourceOp = "Blur1",
					Source = "XBlurSize",
					Default = 0,
				},
				BlurY = InstanceInput {
					SourceOp = "Blur1",
					Source = "YBlurSize",
					Default = 0,
				},
				Blank3 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				GainRed = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GainRed",
					Name = "Core Color",
					ControlGroup = 1,
					Page = "Controls",
					Default = 1,
				},
				GainGreen = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GainGreen",
					ControlGroup = 1,
					Page = "Controls",
					Default = 1,
				},
				GainBlue = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GainBlue",
					ControlGroup = 1,
					Page = "Controls",
					Default = 1,
				},
				GainAlpha = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GainAlpha",
					ControlGroup = 1,
					Page = "Controls",
					Default = 1,
				},
				Blank4 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				LinearOffset = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "LinearOffset",
					Default = 0,
				},
				XSize = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "XSize",
					Name = "Size Variation",
					MaxScale = 1.1,
					Default = 1.05,
				},
				Opacity = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "LayerBlend",
					Name = "Opacity Variation",
					Default = 1,
				},
				Blank5 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				Aberrations = InstanceInput {
					SourceOp = "IRIS",
					Source = "EnableAberrations",
					Page = "Controls",
					Default = 0,
				},
				Diffraction = InstanceInput {
					SourceOp = "DirectionalBlur1",
					Source = "Diffraction",
					Page = "Controls",
					Default = 0,
				},
				AberrationsBlend = InstanceInput {
					SourceOp = "BrightnessContrast1",
					Source = "Blend",
					Name = "Occlusion Amount",
					Default = 1,
				},
				SoftEdge = InstanceInput {
					SourceOp = "Bitmap1",
					Source = "SoftEdge",
					Name = "Occlusion Soft",
				},
				Blank2 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				JitterAxis = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterAxis",
					Name = "Jitter Position",
					DefaultX = 0,
					DefaultY = 0,
				},
				JitterXSize = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterXSize",
					Name = "Jitter Size",
					Default = 0.8,
				},
				JitterAngle = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterAngle",
					Name = "Jitter Angle",
					Default = 0,
				},
				JitterRedGain = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterRedGain",
					Name = "Jitter Red",
					Default = 0,
				},
				JitterGreenGain = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterGreenGain",
					Name = "Jitter Green",
					Default = 0,
				},
				JitterBlueGain = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterBlueGain",
					Name = "Jitter Blue",
					Default = 0,
				},
				JitterAlphaGain = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterAlphaGain",
					Name = "Jitter Alpha",
					Default = 0,
				},
				JitterLayerBlend = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterLayerBlend",
					Name = "Jitter Opacity",
					Default = 0,
				},
				Blank6 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank2",
				},
				Randomize = InstanceInput {
					SourceOp = "Merge4",
					Source = "Randomize",
					Page = "Controls",
					Default = 26024,
				},
				InvertMask = InstanceInput {
					SourceOp = "MASK_In",
					Source = "ApplyMaskInverted",
					Page = "Common",
				},
				MaskChannel = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskChannel",
					Default = 3,
				},
				MaskLow = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskLow",
					ControlGroup = 11,
					Default = 0,
				},
				MaskHigh = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskHigh",
					ControlGroup = 11,
					Default = 1,
				},
				Black = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskClipBlack",
					Name = "Black",
					Default = 1,
				},
				White = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskClipWhite",
					Name = "White",
					Default = 1,
				},
				EffectMask = InstanceInput {
					SourceOp = "MASK_In",
					Source = "EffectMask",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "Merge4",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 426, 98.2727 },
				Flags = {
					Expanded = true,
					AllowPan = false,
					ForceModes = false,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 630.98, 472.557, 344.49, 127.879 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 0, 0 }
			},
			Tools = ordered() {
				LinearCloner1 = Fuse.LinearCloner {
					CtrlWShown = false,
					Inputs = {
						Width = Input {
							Value = 1920,
							Expression = "Merge4.Background.OriginalWidth",
						},
						Height = Input {
							Value = 1080,
							Expression = "Merge4.Background.OriginalHeight",
						},
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						ClonesCount = Input { Value = 25, },
						Point1 = Input {
							Value = { 0.784564131251595, 0.723181241646383 },
							Expression = "IRIS:GetSourceTool(\"Controls\").PointIn1",
						},
						Point2 = Input {
							Value = { 0.5, 0.723181241646383 },
							Expression = "Offset",
						},
						WriteOnStart = Input { Value = -1, },
						XSize = Input { Value = 1.05, },
						AlphaGain = Input { Value = 0, },
						RandomSeed = Input {
							Value = 26024,
							Expression = "Merge4.Randomize",
						},
						JitterXSize = Input { Value = 0.8, },
						JitterGainNest = Input { Value = 1, },
						Clone = Input {
							SourceOp = "Scale1",
							Source = "Output",
						},
						Offset = Input {
							Value = { 0.5, 0.723181241646383 },
							Expression = "Point(IRIS:GetSourceTool(\"Controls\").PointIn2.X,IRIS:GetSourceTool(\"Controls\").PointIn1.Y)",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 223, 169.751 } },
					UserControls = ordered() {
						Offset = {
							LINKS_Name = "Offset",
							LINKID_DataType = "Point",
							INPID_InputControl = "OffsetControl",
							ICS_ControlPage = "Controls"
						}
					}
				},
				DirectionalBlur1 = DirectionalBlur {
					CtrlWShown = false,
					Inputs = {
						Blend = Input {
							Value = 0,
							Expression = "math.ceil(Length)",
						},
						Type = Input { Value = 3, },
						Center = Input { Expression = "IRIS:GetSourceTool(\"Controls\").PointIn2", },
						Length = Input { Expression = "math.abs(IRIS:GetSourceTool(\"Controls\").NumberIn3*Diffraction)", },
						Input = Input {
							SourceOp = "MASK_In",
							Source = "Output",
						},
						Diffraction = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 223, 254.497 } },
					UserControls = ordered() {
						Diffraction = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 5,
							INP_Default = 0.5,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							ICD_Center = 0.5,
							LINKS_Name = "Diffraction"
						}
					}
				},
				Merge4 = Merge {
					CtrlWShown = false,
					Inputs = {
						Foreground = Input {
							SourceOp = "DirectionalBlur1",
							Source = "Output",
						},
						Gain = Input { Value = 0, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 223, 311.642 } },
					UserControls = ordered() {
						Randomize = {
							INP_Integer = true,
							LINKS_Name = "Randomize",
							INPID_InputControl = "SliderControl",
							INP_MinScale = 0,
							LINKID_DataType = "Number",
							INP_MaxScale = 32767,
							INP_Default = 26024,
						}
					}
				},
				MASK_In = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						MultiplyByMask = Input { Value = 1, },
						Alpha = Input { Value = 1, },
						ClipBlack = Input { Value = 1, },
						Input = Input {
							SourceOp = "LinearCloner1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 223, 207.418 } },
				},
				Scale1 = Scale {
					CtrlWShown = false,
					Inputs = {
						HiQOnly = Input { Value = 0, },
						PixelAspect = Input { Value = { 1, 1 }, },
						Input = Input {
							SourceOp = "BrightnessContrast1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 92.565, 169.751 } },
				},
				BrightnessContrast1 = BrightnessContrast {
					CtrlWShown = false,
					Inputs = {
						EffectMask = Input {
							SourceOp = "Bitmap1",
							Source = "Mask",
						},
						ApplyMaskInverted = Input { Value = 1, },
						Alpha = Input { Value = 1, },
						Gain = Input { Value = 0, },
						Input = Input {
							SourceOp = "Instance_Transform3",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -47.715, 169.751 } },
				},
				Bitmap1 = BitmapMask {
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Image = Input {
							SourceOp = "Instance_Transform3",
							Source = "Output",
						},
						Center = Input {
							Value = { 0.50170738478751, 0.501339087449878 },
							Expression = "IRIS:GetSourceTool(\"Controls\").OcRig",
						},
						Channel = Input { Value = FuID { "Luminance" }, },
						High = Input { Value = 0.03, },
					},
					ViewInfo = OperatorInfo { Pos = { -47.715, 108.616 } },
				},
				Transform3 = Transform {
					CtrlWShown = false,
					Inputs = {
						Blend = Input { Expression = "IRIS.EnableAberrations", },
						ProcessRed = Input { Value = 0, },
						ProcessBlue = Input { Value = 0, },
						Center = Input {
							Value = { 0.513452908693346, 0.512820356772324 },
							Expression = "IRIS:GetSourceTool(\"Controls\").AbRig",
						},
						Input = Input {
							SourceOp = "Blur1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -223.98, 127.702 } },
				},
				Instance_Transform3 = Transform {
					CtrlWShown = false,
					SourceOp = "Transform3",
					Inputs = {
						EffectMask = Input { },
						SettingsNest = Input { },
						Blend = Input { Expression = "IRIS.EnableAberrations" },
						ProcessRed = Input { Value = 0, },
						ProcessGreen = Input { Value = 0, },
						ProcessBlue = Input { },
						TransformNest = Input { },
						Center = Input {
							Value = { 0.504590892490801, 0.503571687157682 },
							Expression = "IRIS:GetSourceTool(\"Controls\").AbRig"
						},
						InvertTransform = Input { Value = 1, },
						ReferenceSize = Input { },
						Input = Input {
							SourceOp = "Transform3",
							Source = "Output",
						},
						CommentsNest = Input { },
						FrameRenderScriptNest = Input { },
						StartRenderScripts = Input { },
						EndRenderScripts = Input { },
					},
					ViewInfo = OperatorInfo { Pos = { -223.98, 169.751 } },
				},
				IRIS = Background {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Width = Input {
							Value = 240,
							Expression = "Merge4.Background.OriginalWidth/8",
						},
						Height = Input {
							Value = 135,
							Expression = "Merge4.Background.OriginalHeight/8",
						},
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						BackgroundNest = Input { Value = 0, },
						TopLeftAlpha = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -223.98, -74.5512 } },
					UserControls = ordered() {
						Controls = {
							LINKID_DataType = "Image",
							INP_Default = 0,
							IC_ControlPage = 0,
							INPID_InputControl = "ImageControl",
							LINKS_Name = "Controls",
						},
						EnableAberrations = {
							CBC_TriState = false,
							INP_Integer = false,
							LINKID_DataType = "Number",
							INPID_InputControl = "CheckboxControl",
							LINKS_Name = "Enable Aberrations",
						}
					}
				},
				C_In = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Blend = Input { Value = 0.5, },
						Background = Input {
							SourceOp = "IRIS",
							Source = "Output",
						},
						Size = Input {
							Value = 0.0703125,
							Expression = "IRIS.Height/self.Foreground.OriginalWidth",
						},
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -223.98, -23.3694 } },
				},
				ColorGain1 = ColorGain {
					CtrlWShown = false,
					Inputs = {
						ColorRanges = Input {
							Value = ColorCurves {
								Curves = {
									{
										Points = {
											{ 0, 1 },
											{ 0.125, 0.75 },
											{ 0.375, 0.25 },
											{ 0.5, 0 }
										}
									},
									{
										Points = {
											{ 0.5, 0 },
											{ 0.625, 0.25 },
											{ 0.875, 0.75 },
											{ 1, 1 }
										}
									}
								}
							},
						},
						Input = Input {
							SourceOp = "C_In",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -223.98, 24.9062 } },
					UserControls = ordered() {
						GainRed = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HLS",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 0,
							IC_ControlGroup = 1,
							CLRC_ShowWheel = true,
							LINKS_Name = "Gain Red",
						},
						GainGreen = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HLS",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 1,
							IC_ControlGroup = 1,
							CLRC_ShowWheel = false,
							LINKS_Name = "Gain Green",
						},
						GainBlue = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HLS",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 2,
							IC_ControlGroup = 1,
							CLRC_ShowWheel = true,
							LINKS_Name = "Gain Blue",
						},
						GainAlpha = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HLS",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 3,
							IC_ControlGroup = 1,
							CLRC_ShowWheel = true,
							LINKS_Name = "Gain Alpha",
						}
					}
				},
				Blur1 = Blur {
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						XBlurSize = Input { Value = 0, },
						Input = Input {
							SourceOp = "ColorGain1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -223.98, 73.9115 } },
				}
			},
		}
	},
	ActiveTool = "ml_LFCustomAnaIris"
}