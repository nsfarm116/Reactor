--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_LFStar = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				Input = InstanceInput {
					SourceOp = "STAR",
					Source = "Background",
				},
				Controls = InstanceInput {
					SourceOp = "STAR",
					Source = "Controls",
					Default = 0,
				},
				Blank1 = InstanceInput {
					SourceOp = "STAR",
					Source = "Blank1",
				},
				BlendClone = InstanceInput {
					SourceOp = "STAR",
					Source = "BlendClone",
					Name = "Main Blend",
					Default = 1,
				},
				Copies = InstanceInput {
					SourceOp = "Duplicate2",
					Source = "Copies",
					Name = "Star Sides",
					Page = "Controls",
					Default = 2,
				},
				StarSize = InstanceInput {
					SourceOp = "StarTransform",
					Source = "StarSize",
					Default = 2,
				},
				StarAngle = InstanceInput {
					SourceOp = "StarTransform",
					Source = "StarAngle",
					Default = 0,
				},
				SoftEdge = InstanceInput {
					SourceOp = "StarTransform",
					Source = "SoftEdge",
					Name = " Star Blur",
					MaxScale = 0.03,
					Default = 0,
				},
				Thickness = InstanceInput {
					SourceOp = "StarTransform",
					Source = "StarHeight",
					Name = "Thickness",
					MaxScale = 0.02,
					Default = 0.002,
				},
				Blank2 = InstanceInput {
					SourceOp = "STAR",
					Source = "Blank1",
				},
				TopLeftRed = InstanceInput {
					SourceOp = "StarColor",
					Source = "TopLeftRed",
					ControlGroup = 14,
					Default = 1,
				},
				TopLeftGreen = InstanceInput {
					SourceOp = "StarColor",
					Source = "TopLeftGreen",
					ControlGroup = 14,
					Default = 1,
				},
				TopLeftBlue = InstanceInput {
					SourceOp = "StarColor",
					Source = "TopLeftBlue",
					ControlGroup = 14,
					Default = 0.6,
				},
				TopLeftAlpha = InstanceInput {
					SourceOp = "StarColor",
					Source = "TopLeftAlpha",
					ControlGroup = 14,
					Default = 1,
				},
				Blank3 = InstanceInput {
					SourceOp = "STAR",
					Source = "Blank1",
				},
				JitterXSize = InstanceInput {
					SourceOp = "Duplicate2",
					Source = "JitterXSize",
					Name = "Jitter Size",
					MaxScale = 1,
					Default = 0,
				},
				JitterAngle = InstanceInput {
					SourceOp = "Duplicate2",
					Source = "JitterAngle",
					Name = "Jitter Angle",
					Default = 0,
				},
				JitterLayerBlend = InstanceInput {
					SourceOp = "Duplicate2",
					Source = "JitterLayerBlend",
					Name = "Jitter Opacity",
					Default = 0,
				},
				Random = InstanceInput {
					SourceOp = "Duplicate2",
					Source = "Random",
					Default = 26042,
				},
				Blank4 = InstanceInput {
					SourceOp = "STAR",
					Source = "Blank1",
				},
				CoreBlur = InstanceInput {
					SourceOp = "CORE",
					Source = "SoftEdge",
					Name = "Core Blur",
					Default = 0.016,
				},
				WidthCopy = InstanceInput {
					SourceOp = "CORE",
					Source = "WidthCopy",
					Page = "Controls",
					Default = 0.03,
				},
				HeightCopy = InstanceInput {
					SourceOp = "CORE",
					Source = "HeightCopy",
					Page = "Controls",
					Default = 0.03,
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "STAR",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 536.667, -36.273 },
				Flags = {
					Expanded = true,
					AllowPan = false,
					ForceModes = false,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 874.252, 261.991, 437.126, 23.5167 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 0, -2.17723 }
			},
			Tools = ordered() {
				Star_1 = RectangleMask {
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						SoftEdge = Input { Expression = "StarTransform.SoftEdge", },
						MaskWidth = Input { Value = 800, },
						MaskHeight = Input { Value = 800, },
						PixelAspect = Input { Value = { 1, 1 }, },
						Depth = Input { Value = 4, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Width = Input {
							Value = 1,
							Expression = "StarTransform.StarWidth",
						},
						Height = Input {
							Value = 0.002,
							Expression = "StarTransform.StarHeight",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -374.252, 15.721 } },
					UserControls = ordered() {
						Width = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 1,
							INP_Default = 0.5,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Width",
						},
						Height = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 1,
							INP_Default = 0.5,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Height",
						}
					}
				},
				Ellipse2 = EllipseMask {
					Inputs = {
						EffectMask = Input {
							SourceOp = "Star_1",
							Source = "Mask",
						},
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						SoftEdge = Input { Value = 0.2, },
						PaintMode = Input { Value = FuID { "Minimum" }, },
						OutputSize = Input { Value = FuID { "Custom" }, },
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Width = Input { Value = 0.1591211716196, },
						Height = Input { Value = 0.1591211716196, },
					},
					ViewInfo = OperatorInfo { Pos = { -374.252, 55.0084 } },
				},
				Rectangle1 = RectangleMask {
					Inputs = {
						EffectMask = Input {
							SourceOp = "Ellipse2",
							Source = "Mask",
						},
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						PaintMode = Input { Value = FuID { "Minimum" }, },
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Center = Input { Value = { 0.25, 0.5 }, },
						Height = Input { Value = 1, },
					},
					ViewInfo = OperatorInfo { Pos = { -374.252, 100.333 } },
				},
				CORE = EllipseMask {
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "Rectangle1",
							Source = "Mask",
						},
						Level = Input { Value = 0.282, },
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						SoftEdge = Input { Value = 0.016, },
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Width = Input {
							Value = 0.03,
							Expression = "WidthCopy",
						},
						Height = Input {
							Value = 0.03,
							Expression = "HeightCopy",
						},
						WidthCopy = Input { Value = 0.03, },
						HeightCopy = Input { Value = 0.03, }
					},
					ViewInfo = OperatorInfo { Pos = { -374.252, 143.268 } },
					UserControls = ordered() {
						WidthCopy = {
							INP_Integer = false,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							INPID_InputControl = "SliderControl",
							LINKS_Name = "WidthCopy"
						},
						HeightCopy = {
							INP_Integer = false,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							INPID_InputControl = "SliderControl",
							LINKS_Name = "HeightCopy"
						}
					}
				},
				StarColor = Background {
					CtrlWShown = false,
					Inputs = {
						EffectMask = Input {
							SourceOp = "CORE",
							Source = "Mask",
						},
						Width = Input {
							Value = 480,
							Expression = "STAR.Background.OriginalWidth/4",
						},
						Height = Input {
							Value = 480,
							Expression = "Width",
						},
						Depth = Input { Value = 3, },
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						TopLeftRed = Input { Value = 1, },
						TopLeftGreen = Input { Value = 1, },
						TopLeftBlue = Input { Value = 0.6, },
						GradientType = Input { Value = FuID { "Radial" }, },
						Start = Input { Value = { 0.5, 0.5 }, },
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 0, 1.52, 5, 1 }
								}
							},
						},
					},
					ViewInfo = OperatorInfo { Pos = { -374.252, 205.673 } },
				},
				Duplicate2 = Fuse.Duplicate {
					CtrlWShown = false,
					Inputs = {
						Angle = Input {
							Value = 72,
							Expression = "360/Copies",
						},
						BurnIn = Input { Value = 1, },
						RandomSeed = Input {
							Value = 26024,
							Expression = "Random",
						},
						Background = Input {
							SourceOp = "StarColor",
							Source = "Output",
						},
						Copies = Input { Value = 2, },
					},
					ViewInfo = OperatorInfo { Pos = { -186.388, 205.673 } },
					UserControls = ordered() {
						Copies = {
							INP_MaxAllowed = 1000000,
							INP_Integer = true,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 10,
							INP_Default = 5,
							INP_MinScale = 2,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							LINKS_Name = "Copies"
						},
						Random = {
							INP_MaxAllowed = 1000000,
							INP_Integer = true,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 35000,
							INP_Default = 26042,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Random",
						}
					}
				},
				Transform31 = Transform {
					CtrlWShown = false,
					Inputs = {
						Size = Input {
							Value = 3,
							Expression = "StarTransform.StarSize",
						},
						Angle = Input { Expression = "StarTransform.StarAngle", },
						FlattenTransform = Input { Value = 1, },
						Input = Input {
							SourceOp = "Duplicate2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -1.34304, 205.673 } },
				},
				StarTransform = ChangeDepth {
					CtrlWShown = false,
					Inputs = {
						Input = Input {
							SourceOp = "Transform31",
							Source = "Output",
						},
						StarSize = Input { Value = 2, },
						StarHeight = Input { Value = 0.002, }
					},
					ViewInfo = OperatorInfo { Pos = { 181.428, 205.673 } },
					UserControls = ordered() {
						StarSize = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 5,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Star Size",
						},
						StarAngle = {
							INP_Integer = false,
							LINKID_DataType = "Number",
							LINKS_Name = "Star Angle",
							INPID_InputControl = "ScrewControl",
							INP_MaxScale = 90,
							INP_Default = 0,
						},
						SoftEdge = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 1,
							INP_Default = 0,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "SoftEdge",
						},
						StarWidth = {
							INP_Integer = false,
							LINKID_DataType = "Number",
							INP_Default = 1,
							INPID_InputControl = "SliderControl",
							LINKS_Name = "StarWidth",
						},
						StarHeight = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 0.0199999995529652,
							INP_Default = 0.00249999994412065,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "StarHeight",
						}
					}
				},
				STAR = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Foreground = Input {
							SourceOp = "StarTransform",
							Source = "Output",
						},
						Center = Input {
							Value = { 0.3, 0.7 },
							Expression = "self:GetSourceTool(\"Controls\").PointIn1",
						},
						Gain = Input { Value = 0, },
						FilterMethod = Input { Value = 0, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 374, 205.673 } },
					UserControls = ordered() {
						Controls = {
							LINKID_DataType = "Image",
							INP_Default = 0,
							IC_ControlPage = 0,
							INPID_InputControl = "ImageControl",
							LINKS_Name = "Controls",
						}
					}
				}
			},
		}
	},
	ActiveTool = "ml_LFStar"
}