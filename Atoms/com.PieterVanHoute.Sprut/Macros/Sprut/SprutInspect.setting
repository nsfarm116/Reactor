{
	Tools = ordered() {
		SprutInspect = MacroOperator {
			NameSet = true,
			Inputs = ordered() {
				Comments = Input { Value = "Sprut2 for Fusion\n\n-------------------------------------------------------------------\nCopyright (c) 2020,  Pieter Van Houte\n<pieter[at]secondman[dot]com>\n-------------------------------------------------------------------\n\nSprut (c) 2012-2014 Theodor Groeneboom - www.euqahuba.com (theo@euqahuba.com)\n\nSprut2 is distributed under GNU/GPL. It may be used for commercial purposes, but not redistributed or repackaged, in particular not for any fee.", },
				Solver = InstanceInput {
					SourceOp = "SolverIn",
					Source = "Background",
				},
				View = InstanceInput {
					SourceOp = "SolverIn",
					Source = "View",
					Default = 1,
				},
				InvertVectors = InstanceInput {
					SourceOp = "SolverIn",
					Source = "InvertVectors",
					Default = 0,
				},
				StepSize = InstanceInput {
					SourceOp = "SolverIn",
					Source = "StepSize",
					Default = 12,
				},
				PreBlurVectors = InstanceInput {
					SourceOp = "SolverIn",
					Source = "PreBlurVectors",
					Default = 2,
				},
				ScaleVectors = InstanceInput {
					SourceOp = "SolverIn",
					Source = "ScaleVectors",
					Default = 5,
				},
				Upscaling = InstanceInput {
					SourceOp = "SolverIn",
					Source = "Upscaling",
					Default = 1,
				}
			},
			Outputs = {
				Output = InstanceOutput {
					SourceOp = "Inspect",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo { Pos = { -110, -3019.5 } },
			Tools = ordered() {
				Inspect = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "BrightnessContrast1_1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "SolverIn",
							Source = "Output",
						},
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 55, 338.758 } },
				},
				Blur2 = Blur {
					CtrlWShown = false,
					Inputs = {
						Filter = Input { Value = FuID { "Gaussian" }, },
						XBlurSize = Input {
							Value = 2,
							Expression = "SolverIn.PreBlurVectors",
						},
						Input = Input {
							SourceOp = "Scale1_1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 55, 206.758 } },
				},
				VectorsToRG = ChannelBoolean {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						ToRed = Input { Value = 31, },
						ToGreen = Input { Value = 32, },
						ToBlue = Input { Value = 15, },
						ToAlpha = Input { Value = 15, },
						Background = Input {
							SourceOp = "CustomTool1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 55, 107.758 } },
				},
				CustomTool1 = Custom {
					CtrlWShown = false,
					Inputs = {
						PointIn1 = Input {
							Value = { 960, 540 },
							Expression = "Point(Image1.OriginalWidth, Image1.OriginalHeight)",
						},
						NumberIn1 = Input {
							Value = 12,
							Expression = "SolverIn.StepSize",
						},
						NumberIn2 = Input {
							Value = 1,
							Expression = "SolverIn.View",
						},
						LUTIn1 = Input {
							SourceOp = "CustomTool1LUTIn1",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "CustomTool1LUTIn2",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "CustomTool1LUTIn3",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "CustomTool1LUTIn4",
							Source = "Value",
						},
						Setup1 = Input { Value = "\n", },
						Intermediate1 = Input { Value = "max((1-((y*h+0.5)%n1-0.5)),0) * max((1-((x*w+0.5)%n1-0.5)),0)", },
						RedExpression = Input { Value = "if(n2==0,i1*c1,i1)", },
						GreenExpression = Input { Value = "if(n2==0,i1*c1,i1)", },
						BlueExpression = Input { Value = "if(n2==0,i1*c1,i1)", },
						AlphaExpression = Input { Value = "if(n2==0,i1*c1,i1)", },
						NumberControls = Input { Value = 1, },
						NameforNumber1 = Input { Value = "Step Size", },
						Image1 = Input {
							SourceOp = "SolverIn",
							Source = "Output",
						},
						Image2 = Input {
							SourceOp = "Blur3",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -55, 107.758 } },
				},
				Blur3 = Blur {
					CtrlWShown = false,
					Inputs = {
						Filter = Input { Value = FuID { "Gaussian" }, },
						XBlurSize = Input { Value = 60, },
						Input = Input {
							SourceOp = "SolverIn",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 55, 74.7576 } },
				},
				ChannelBooleans8 = ChannelBoolean {
					CtrlWShown = false,
					Inputs = {
						ToRed = Input { Value = 4, },
						ToGreen = Input { Value = 4, },
						ToBlue = Input { Value = 4, },
						ToAlpha = Input { Value = 4, },
						EnableExtraChannels = Input { Value = 1, },
						ToXVector = Input { Value = 0, },
						ToYVector = Input { Value = 1, },
						Background = Input {
							SourceOp = "CustomTool1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Blur2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -55, 206.758 } },
				},
				VectorMotionBlur1 = VectorMotionBlur {
					CtrlWShown = false,
					Inputs = {
						Input = Input {
							SourceOp = "ChannelBooleans8",
							Source = "Output",
						},
						FlipX = Input { Expression = "SolverIn.InvertVectors", },
						FlipY = Input { Expression = "SolverIn.InvertVectors", },
						XScale = Input { Expression = "SolverIn.ScaleVectors", },
					},
					ViewInfo = OperatorInfo { Pos = { -55, 272.758 } },
				},
				SolverIn = ChannelBoolean {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						View = Input { Value = 1, },
					},
					ViewInfo = OperatorInfo { Pos = { -55, 8.75757 } },
					UserControls = ordered() {
						View = {
							{ MBTNC_AddButton = "Color" },
							{ MBTNC_AddButton = "White" },
							LINKID_DataType = "Number",
							INP_Integer = false,
							MBTNC_ShowBasicButton = false,
							INPID_InputControl = "MultiButtonControl",
							MBTNC_ShowName = false,
							MBTNC_StretchToFit = false,
							MBTNC_ShowToolTip = false,
							LINKS_Name = "View",
						},
						InvertVectors = {
							CBC_TriState = false,
							INP_Integer = false,
							LINKID_DataType = "Number",
							LINKS_Name = "Invert Vectors",
							INPID_InputControl = "CheckboxControl",
							INP_Default = 0,
						},
						StepSize = {
							INP_Integer = true,
							LINKS_Name = "Step Size",
							INPID_InputControl = "SliderControl",
							INP_MinScale = 1,
							LINKID_DataType = "Number",
							INP_MaxScale = 24,
							INP_Default = 12,
						},
						PreBlurVectors = {
							INP_Integer = false,
							LINKS_Name = "Pre Blur Vectors",
							INPID_InputControl = "SliderControl",
							INP_MinScale = 0,
							LINKID_DataType = "Number",
							INP_MaxScale = 10,
							INP_Default = 2,
						},
						ScaleVectors = {
							INP_Integer = false,
							LINKS_Name = "Scale Vectors",
							INPID_InputControl = "SliderControl",
							INP_MinScale = 1,
							LINKID_DataType = "Number",
							INP_MaxScale = 10,
							INP_Default = 5,
						},
						Upscaling = {
							INP_MaxAllowed = 1000000,
							INP_Integer = true,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 4,
							INP_Default = 1,
							INP_MinScale = 1,
							INP_MinAllowed = 1,
							LINKID_DataType = "Number",
							LINKS_Name = "Upscaling (Match Solver)",
						}
					}
				},
				Crop1_1 = Crop {
					CtrlWShown = false,
					Inputs = {
						UseGPU = Input { Expression = "EmitterIn.UseGPUFu16", },
						XSize = Input {
							Value = 960,
							Expression = "self.Input.OriginalWidth/SolverIn.Upscaling",
						},
						YSize = Input {
							Value = 540,
							Expression = "self.Input.OriginalHeight/SolverIn.Upscaling",
						},
						Input = Input {
							SourceOp = "VectorsToRG",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 55, 140.758 } },
				},
				Scale1_1 = Scale {
					CtrlWShown = false,
					Inputs = {
						UseGPU = Input { Expression = "EmitterIn.UseGPUFu16", },
						XSize = Input { Expression = "SolverIn.Upscaling", },
						HiQOnly = Input { Value = 0, },
						PixelAspect = Input { Value = { 1, 1 }, },
						FilterMethod = Input { Value = 0, },
						Input = Input {
							SourceOp = "Crop1_1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 55, 173.758 } },
				},
				BrightnessContrast1_1 = BrightnessContrast {
					CtrlWShown = false,
					Inputs = {
						Gain = Input { Value = 40, },
						Gamma = Input { Value = 1.94, },
						Input = Input {
							SourceOp = "VectorMotionBlur1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -55, 338.758 } },
				}
			},
		},
		CustomTool1LUTIn1 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 0, Blue = 0 },
		},
		CustomTool1LUTIn2 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 204, Blue = 0 },
		},
		CustomTool1LUTIn3 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 0, Blue = 204 },
		},
		CustomTool1LUTIn4 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 204, Blue = 204 },
		}
	}
}