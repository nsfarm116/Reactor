DEBUG = false
print("Animanager v0.1")

--[[-- 
	Animagus Script for Fusion
	v0.1, 2020-01-08
	by Bryan Ray for Muse VFX

	=====Overview=====

	Tools with numerous animated inputs can be difficult to manage. This script can run in several modes:
	1) Remove all animation
	2) Remove keyframe from all animated inputs at the current frame.
	3) Add a keyframe to all animated inputs at the current frame.
	4) Add animation to all animatable inputs (and set a key at the current frame)

--]]--

-- ===========================================================================
-- globals
-- ===========================================================================
_fusion = nil
_comp = nil
ui = nil
disp = nil



--==============================================================
-- Main()
--
-- Typical set-up for a script developed in an IDE. Sets globals
-- and connects to the active comp. Initializes UI Manger.
--==============================================================

function main()
	-- get fusion instance
	_fusion = getFusion()

	-- ensure a fusion instance was retrieved
	if not _fusion then
		error("Please open the Fusion GUI before running this tool.")
	end

	-- Set up aliases to the UI Manager framework
	--ui = _fusion.UIManager
	--disp = bmd.UIDispatcher(ui)
	
	-- get composition
	_comp = _fusion.CurrentComp
	SetActiveComp(_comp)

	-- ensure a composition is active
	if not _comp then
		error("Please open a composition before running this tool.")
	end

	dprint("\nActive comp is ".._comp:GetAttrs().COMPS_Name)

	-- Ensure a tool is active
	dprint("Tool: ")
	ddump(tool)
	if not tool and _comp.ActiveTool then 
		tool = _comp.ActiveTool 
	elseif not tool then
		local error = _comp:AskUser("ERROR: No tool selected", {})
		return		
	end

	-- Ask User which mode?
	-- Set Keys here
	-- Remove Keys here
	-- Remove all animation
	-- Animate everything
	local d = {}
	local modeList = {
		[0] = "Set Key on All Animated Inputs", 
		[1] = "Remove Keys from All Animated Inputs", 
		[2] = "Remove All Animation",
		[3] = "Animate all Inputs",
	}
	d[1] = {"Menu", Name = "Mode", "Dropdown", Options = modeList, default = 0}

	local mode = _comp:AskUser("Choose the Form of the Destructor", d).Menu

	if mode == 0 then -- Set Keys
		dprint("Set key on all animated inputs")
		local animatedInputs = {}
		for i, inp in ipairs(tool:GetInputList()) do 								-- Get list of animated inputs.
			local toolType
			if inp:GetConnectedOutput() then toolType = inp:GetConnectedOutput():GetTool().ID end
			if toolType == 'BezierSpline' or toolType == 'PolyPath' then 	-- For each animated input,
				inp[_comp.CurrentTime] = inp[_comp.CurrentTime]								-- Set key
			end
		end

	elseif mode == 1 then -- Remove Keys
		dprint("Remove keys from all animated inputs")
		for i, inp in ipairs(tool:GetInputList()) do								-- Get list of animated inputs.
			if inp:GetConnectedOutput() then toolType = inp:GetConnectedOutput():GetTool().ID end
			if toolType == 'BezierSpline' or toolType == 'PolyPath' then 	-- For each animated input,
				inp[_comp.CurrentTime] = nil 								-- If key on this frame, Delete key
			end
		end
			
	elseif mode == 2 then -- Remove all animation
		dprint("Remove all animation")
		for i, inp in ipairs(tool:GetInputList()) do								-- Get list of animated inputs.
			local toolType
			if inp:GetConnectedOutput() then toolType = inp:GetConnectedOutput():GetTool().ID end
			if toolType == 'BezierSpline' or toolType == 'PolyPath' then 	-- For each animated input,
				--local value = inp[_comp.CurrentTime]						-- Get current value
				inp:ConnectTo(nil) 													-- Remove animation spline
				--inp[_comp.CurrentTime] = value								-- Restore current value
			end
		end

	else -- Animate everything
		dprint("Animate everything")
		for i, inp in ipairs(tool:GetInputList()) do 								-- Get list of animateable inputs
			if inp:GetAttrs().INPB_External == true and not inp:GetConnectedOutput() then
				-- Record current value
				local val = inp[_comp.CurrentTime]
				if inp:GetAttrs().INPS_DataType == 'Point' then
					local newinput = PolyPath({})
					ddump(newinput)
				-- Add an animation spline
					inp:ConnectTo(newinput)
					inp = val
				elseif inp:GetAttrs().INPS_DataType == 'Number' then
					local newinput = BezierSpline({})
					inp:ConnectTo(newinput)
					inp = val
				end
			end
		end
	end
end

--======================== ENVIRONMENT SETUP ============================--

------------------------------------------------------------------------
-- getFusion()
--
-- check if global fusion is set, meaning this script is being
-- executed from within fusion
--
-- Arguments: None
-- Returns: handle to the Fusion instance
------------------------------------------------------------------------
function getFusion()
	if fusion == nil then 
		-- remotely get the fusion ui instance
		fusion = bmd.scriptapp("Fusion", "localhost")
	end
	return fusion
end -- end of getFusion()

--========================== COMP MANIPULATION ============================--

----------------------------------------------------------------------------------------------
-- Fusion can be locked or unlocked multiple times. These functions ensure that the lock state
-- is not nested.
----------------------------------------------------------------------------------------------

-- Unlocks the comp
function unlockComp(c)
	if c:GetAttrs().COMPB_Locked == false then lockComp(c) end
	while c:GetAttrs().COMPB_Locked == true do
		c:Unlock()
	end
end

-- Locks the comp
function lockComp(c)
	if c:GetAttrs().COMPB_Locked == true then unlockComp(c) end
	while c:GetAttrs().COMPB_Locked == false do
		c:Lock()
	end
end


--========================== DEBUGGING ============================--


---------------------------------------------------------------------
-- dprint(string, suppressNewline)
--
-- Prints debugging information to the console when DEBUG flag is set
--
-- Arguments:
--		string, string, a message for the console
--		suppressNewline, boolean, do not start a new line
---------------------------------------------------------------------
function dprint(string, suppressNewline)
	local newline = "\n"
	if suppressNewline then newline = '' end
	if DEBUG and string then _comp:Print(string..newline) end
end -- dprint()

---------------------------------------------------------------------
-- ddump(object)
--
-- Performs a dump() if the DEBUG flag is set
--
-- Arguments
--		object, object, an object to be dumped
---------------------------------------------------------------------
function ddump(object)
	if DEBUG then dump(object) end
end -- end ddump()

main()