--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 2.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--
{
	Tools = ordered() {
		SkinCorrector = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				CommentsNest = Input { Value = 0, },
				Input = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "Input",
				},
				Display = InstanceInput {
					SourceOp = "Display_3",
					Source = "Display",
					Page = "Controls",
					Default = 3,
				},
				Input04 = InstanceInput {
					SourceOp = "COMMON",
					Source = "Blank1",
				},
				Filter = InstanceInput {
					SourceOp = "BLUR",
					Source = "Filter",
				},
				Softness = InstanceInput {
					SourceOp = "Softness",
					Source = "XBlurSize",
					Name = "Skin Softness",
					Default = 10,
				},
				Detail = InstanceInput {
					SourceOp = "BLUR",
					Source = "XBlurSize",
					Name = "Skin Detail",
					MaxScale = 5,
					Default = 1,
				},
				Mix = InstanceInput {
					SourceOp = "MIX",
					Source = "BlendClone",
					Name = "Mix",
					Default = 0.5,
				},
				Input05 = InstanceInput {
					SourceOp = "COMMON",
					Source = "Blank1",
				},
				Depth = InstanceInput {
					SourceOp = "ChangeDepth1",
					Source = "Depth",
					Default = 0,
				},
				ColorRange = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "ColorRange",
					Page = "Skin Selection",
				},
				KeyType = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "KeyType",
					Page = "Skin Selection",
				},
				RedLow = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "RedLow",
					Name = "Low",
					ControlGroup = 27,
					Default = 0,
				},
				RedHigh = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "RedHigh",
					Name = "High",
					ControlGroup = 27,
					Default = 1,
				},
				GreenLow = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "GreenLow",
					Name = "Low",
					ControlGroup = 28,
					Default = 0,
				},
				GreenHigh = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "GreenHigh",
					Name = "High",
					ControlGroup = 28,
					Default = 1,
				},
				BlueLow = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "BlueLow",
					Name = "Low",
					ControlGroup = 29,
					Default = 0,
				},
				BlueHigh = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "BlueHigh",
					Name = "High",
					ControlGroup = 29,
					Default = 1,
				},
				LuminanceLow = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "LuminanceLow",
					Name = "Low",
					ControlGroup = 30,
					Default = 0,
				},
				LuminanceHigh = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "LuminanceHigh",
					Name = "High",
					ControlGroup = 30,
					Default = 1,
				},
				LockColorPicking = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "LockColorPicking",
					Default = 0,
				},
				SoftRange = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "SoftRange",
				},
				ResetColorRanges = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "ResetColorRanges",
				},
				MatteLabel = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "Matte",
					Page = "Skin Selection",
				},
				FilterSkin = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "Filter",
				},
				MatteBlur = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "MatteBlur",
					Default = 5,
				},
				Passes = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "Passes",
					Default = 4,
				},
				MatteContractExpand = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "MatteContractExpand",
					Default = 0,
				},
				MatteGamma = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "MatteGamma",
					Default = 1,
				},
				LowMatteThreshold = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "LowMatteThreshold",
					ControlGroup = 40,
					Default = 0,
				},
				HighSkin = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "High",
					ControlGroup = 40,
					Default = 1,
				},
				RestoreFringe = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "RestoreFringe",
					Default = 0,
				},
				Input02 = InstanceInput {
					SourceOp = "COMMON",
					Source = "Blank1",
				},
				ClippingMode = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "ClippingMode",
				},
				Label = InstanceInput {
					SourceOp = "Selection",
					Source = "ColorLabel",
					Name = "Selection Color",
					Page = "Skin Selection",
				},
				Red = InstanceInput {
					SourceOp = "Selection",
					Source = "NumberIn1",
					ControlGroup = 2,
					Page = "Skin Selection",
					Default = 0,
				},
				Green = InstanceInput {
					SourceOp = "Selection",
					Source = "NumberIn2",
					Name = "Green",
					ControlGroup = 2,
					Page = "Skin Selection",
					Default = 0.2,
				},
				Blue = InstanceInput {
					SourceOp = "Selection",
					Source = "NumberIn3",
					Name = "Blue",
					ControlGroup = 2,
					Page = "Skin Selection",
					Default = 0.5,
				},
				Alpha = InstanceInput {
					SourceOp = "Selection",
					Source = "NumberIn4",
					Name = "Alpha",
					ControlGroup = 2,
					Page = "Skin Selection",
					Default = 0.5,
				},
				DetailAmount = InstanceInput {
					SourceOp = "DetailRecover",
					Source = "XBlurSize",
					Name = "Detail Amount",
					Page = "Skin Details",
					Default = 8,
				},
				SoftEdgeDt = InstanceInput {
					SourceOp = "DetailMask",
					Source = "SoftEdge",
				},
				LowDt = InstanceInput {
					SourceOp = "DetailMask",
					Source = "Low",
					ControlGroup = 21,
					Default = 0,
				},
				HighDt = InstanceInput {
					SourceOp = "DetailMask",
					Source = "High",
					ControlGroup = 21,
					Default = 1,
				},
				BlackDt = InstanceInput {
					SourceOp = "DetailMask",
					Source = "ClipBlack",
					Name = "Black",
					Default = 1,
				},
				WhiteDt = InstanceInput {
					SourceOp = "DetailMask",
					Source = "ClipWhite",
					Name = "White",
					Default = 1,
				},
				Extension = InstanceInput {
					SourceOp = "Extention",
					Source = "XBlurSize",
					Name = "Skin Extension",
					Page = "Skin Extension",
					Default = 15,
				},
				Blend = InstanceInput {
					SourceOp = "COMMON",
					Source = "Blend",
					Page = "Common",
					Default = 1,
				},
				Input3 = InstanceInput {
					SourceOp = "COMMON",
					Source = "ProcessWhenBlendIs00",
					Default = 0,
				},
				Input4 = InstanceInput {
					SourceOp = "COMMON",
					Source = "ProcessRed",
					Name = "Process",
					ControlGroup = 4,
					Default = 1,
				},
				Input5 = InstanceInput {
					SourceOp = "COMMON",
					Source = "ProcessGreen",
					ControlGroup = 4,
					Default = 1,
				},
				Input6 = InstanceInput {
					SourceOp = "COMMON",
					Source = "ProcessBlue",
					ControlGroup = 4,
					Default = 1,
				},
				Input7 = InstanceInput {
					SourceOp = "COMMON",
					Source = "ProcessAlpha",
					ControlGroup = 4,
					Default = 1,
				},
				Input8 = InstanceInput {
					SourceOp = "COMMON",
					Source = "Blank1",
				},
				Input9 = InstanceInput {
					SourceOp = "COMMON",
					Source = "ApplyMaskInverted",
					Default = 0,
				},
				Input10 = InstanceInput {
					SourceOp = "COMMON",
					Source = "MultiplyByMask",
					Default = 0,
				},
				Input11 = InstanceInput {
					SourceOp = "COMMON",
					Source = "FitMask",
				},
				Input12 = InstanceInput {
					SourceOp = "COMMON",
					Source = "Blank2",
				},
				Input13 = InstanceInput {
					SourceOp = "COMMON",
					Source = "MaskChannel",
					Default = 3,
				},
				Input14 = InstanceInput {
					SourceOp = "COMMON",
					Source = "MaskLow",
					ControlGroup = 11,
					Default = 0,
				},
				Input15 = InstanceInput {
					SourceOp = "COMMON",
					Source = "MaskHigh",
					ControlGroup = 11,
					Default = 1,
				},
				Input16 = InstanceInput {
					SourceOp = "COMMON",
					Source = "MaskClipBlack",
					Name = "Black",
					Default = 1,
				},
				Input17 = InstanceInput {
					SourceOp = "COMMON",
					Source = "MaskClipWhite",
					Name = "White",
					Default = 1,
				},
				EffectMask = InstanceInput {
					SourceOp = "COMMON",
					Source = "EffectMask",
				},
				Matte = InstanceInput {
					SourceOp = "SkinSelection",
					Source = "Solid.Matte",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "Display_3",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 939.333, 168.501 },
				Flags = {
					AllowPan = false,
					ForceModes = false,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 1058.49, 595.943, 529.244, 24.2424 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 0, 0 }
			},
			Tools = ordered() {
				COMMON = Merge {
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "MIX",
							Source = "Output",
						},
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 175.043, 470.205 } },
				},
				Display_3 = Dissolve {
					Transitions = {
						[0] = "DFTDissolve"
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Mix = Input {
							Value = 0,
							Expression = "iif(Display==2,1,0)",
						},
						Background = Input {
							SourceOp = "Display_2",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Softness",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 466, 537.547 } },
					UserControls = ordered() {
						Display = {
							{ CCS_AddString = "Skin Selection" },
							{ CCS_AddString = "Skin Detail" },
							{ CCS_AddString = "Skin Extention" },
							{ CCS_AddString = "Final" },
							INP_Integer = true,
							LINKS_Name = "Display",
							ICS_ControlPage = "Controls",
							INPID_InputControl = "ComboControl",
							CC_LabelPosition = "Horizontal",
							LINKID_DataType = "Number",
							INP_Default = 3
						}
					}
				},
				Display_2 = Dissolve {
					Transitions = {
						[0] = "DFTDissolve"
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Mix = Input {
							Value = 0,
							Expression = "iif(Display_3.Display==1,1,0)",
						},
						Background = Input {
							SourceOp = "Display_1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "CustomTool1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 320.013, 537.547 } },
				},
				Display_1 = Dissolve {
					Transitions = {
						[0] = "DFTDissolve"
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Mix = Input {
							Value = 0,
							Expression = "iif(Display_3.Display==0,1,0)",
						},
						Background = Input {
							SourceOp = "COMMON",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Merge4",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 175.043, 537.547 } },
				},
				Selection = Custom {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "SkinSelection",
							Source = "Output",
						},
						LUTIn1 = Input {
							SourceOp = "SelectionLUTIn1",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "SelectionLUTIn2",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "SelectionLUTIn3",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "SelectionLUTIn4",
							Source = "Value",
						},
						RedExpression = Input { Value = "n1", },
						GreenExpression = Input { Value = "n2", },
						BlueExpression = Input { Value = "n3", },
						AlphaExpression = Input { Value = "n4", },
						NumberControls = Input { Value = 1, },
						NameforNumber1 = Input { Value = "Color", },
						ShowNumber5 = Input { Value = 0, },
						ShowNumber6 = Input { Value = 0, },
						ShowNumber7 = Input { Value = 0, },
						ShowNumber8 = Input { Value = 0, },
						ShowPoint1 = Input { Value = 0, },
						ShowPoint2 = Input { Value = 0, },
						ShowPoint3 = Input { Value = 0, },
						ShowPoint4 = Input { Value = 0, },
						ShowLUT1 = Input { Value = 0, },
						ShowLUT2 = Input { Value = 0, },
						ShowLUT3 = Input { Value = 0, },
						ShowLUT4 = Input { Value = 0, },
						Image1 = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
						NumberIn2 = Input { Value = 0.2, },
						NumberIn3 = Input { Value = 0.5, },
						NumberIn4 = Input { Value = 0.5, },
					},
					ViewInfo = OperatorInfo { Pos = { -42.8099, 485.473 } },
					UserControls = ordered() {
						NumberIn1 = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							IC_ControlGroup = 2,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 0,
							ICS_ControlPage = "Controls",
							CLRC_ShowWheel = true,
							LINKS_Name = "Number In 1"
						},
						NumberIn2 = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							IC_ControlGroup = 2,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 1,
							ICS_ControlPage = "Controls",
							CLRC_ShowWheel = true,
							LINKS_Name = "Number In 2"
						},
						NumberIn3 = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							IC_ControlGroup = 2,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 2,
							ICS_ControlPage = "Controls",
							CLRC_ShowWheel = true,
							LINKS_Name = "Number In 3"
						},
						NumberIn4 = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							IC_ControlGroup = 2,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 3,
							ICS_ControlPage = "Controls",
							CLRC_ShowWheel = true,
							LINKS_Name = "Number In 4"
						},
						ColorLabel = {
							INP_Integer = false,
							LBLC_DropDownButton = true,
							LINKID_DataType = "Number",
							LBLC_NumInputs = 4,
							ICS_ControlPage = "Controls",
							INPID_InputControl = "LabelControl",
							LINKS_Name = "ColorLabel"
						}
					}
				},
				SelectionLUTIn1 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 204, Green = 0, Blue = 0 },
				},
				SelectionLUTIn2 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 0, Green = 204, Blue = 0 },
				},
				SelectionLUTIn3 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 0, Green = 0, Blue = 204 },
				},
				SelectionLUTIn4 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 204, Green = 204, Blue = 204 },
				},
				Merge4 = Merge {
					CtrlWShown = false,
					Inputs = {
						Background = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Selection",
							Source = "Output",
						},
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -42.8099, 537.547 } },
				},
				BLUR = Blur {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						Input = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -205.574, 260.563 } },
				},
				SkinMask = BitmapMask {
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "DetailMask",
							Source = "Mask",
						},
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						PaintMode = Input { Value = FuID { "Multiply" }, },
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						Image = Input {
							SourceOp = "SkinSelection",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 152.71, 217.556 } },
				},
				Subtract = ChannelBoolean {
					NameSet = true,
					Inputs = {
						Operation = Input { Value = 2, },
						ToAlpha = Input { Value = 4, },
						Background = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "BLUR",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -205.574, 315.409 } },
				},
				DetailRecover = Blur {
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "BLUR.Filter",
						},
						XBlurSize = Input { Value = 8, },
						Input = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -207.169, 196.719 } },
				},
				DetailMask = BitmapMask {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "BLUR.Filter",
						},
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						Image = Input {
							SourceOp = "Divide1",
							Source = "Output",
						},
						Channel = Input { Value = FuID { "Luminance" }, },
					},
					ViewInfo = OperatorInfo { Pos = { 302.142, 147.396 } },
				},
				Divide1 = ChannelBoolean {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Operation = Input { Value = 7, },
						ToAlpha = Input { Value = 4, },
						Background = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "DetailRecover",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -207.169, 147.396 } },
				},
				Softness = Blur {
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "BLUR.Filter",
						},
						XBlurSize = Input { Value = 10, },
						Input = Input {
							SourceOp = "Under",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 385.692, 93.0901 } },
				},
				Under = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "Divide",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "SkinSelection",
							Source = "Output",
						},
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 235.563, 93.0901 } },
				},
				SkinSelection = ChromaKeyer {
					NameSet = true,
					Inputs = {
						ColorRange = Input { Value = 1, },
						SpillSuppression = Input { Value = 0, },
						FringeSize = Input { Value = 0, },
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						MatteBlur = Input { Value = 5, },
						InvertMatte = Input { Value = 1, },
						Input = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -207.652, 93.0901 } },
				},
				ChangeDepth1 = ChangeDepth {
					ViewInfo = OperatorInfo { Pos = { -466.487, 398.106 } },
				},
				Divide = ChannelBoolean {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Operation = Input { Value = 7, },
						ToRed = Input { Value = 3, },
						ToGreen = Input { Value = 3, },
						ToBlue = Input { Value = 3, },
						Background = Input {
							SourceOp = "Extention",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 235.563, 7.96814 } },
				},
				Extention = Blur {
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "BLUR.Filter",
						},
						XBlurSize = Input { Value = 15, },
						Input = Input {
							SourceOp = "SkinSelection",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 33.9195, 7.96814 } },
				},
				CustomTool1 = Custom {
					Inputs = {
						LUTIn1 = Input {
							SourceOp = "CustomTool1LUTIn12",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "CustomTool1LUTIn22",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "CustomTool1LUTIn32",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "CustomTool1LUTIn42",
							Source = "Value",
						},
						RedExpression = Input { Value = "a2", },
						GreenExpression = Input { Value = "a2", },
						BlueExpression = Input { Value = "a2", },
						Image1 = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
						Image2 = Input {
							SourceOp = "DetailMask",
							Source = "Mask",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 302.142, 346.301 } },
				},
				ADD = ChannelBoolean {
					NameSet = true,
					Inputs = {
						MultiplyByMask = Input { Value = 1, },
						Operation = Input { Value = 1, },
						ToAlpha = Input { Value = 4, },
						Background = Input {
							SourceOp = "Softness",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Subtract",
							Source = "Output",
						},
						EffectMask = Input {
							SourceOp = "SkinMask",
							Source = "Mask",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 175.043, 315.409 } },
				},
				MIX = Merge {
					NameSet = true,
					Inputs = {
						Blend = Input { Value = 0.5, },
						Background = Input {
							SourceOp = "ChangeDepth1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "ADD",
							Source = "Output",
						},
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 175.043, 398.106 } },
				}
			},
		},
		CustomTool1LUTIn12 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 0, Blue = 0 },
		},
		CustomTool1LUTIn22 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 204, Blue = 0 },
		},
		CustomTool1LUTIn32 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 0, Blue = 204 },
		},
		CustomTool1LUTIn42 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 204, Blue = 204 },
		}
	},
	ActiveTool = "SkinCorrector"
}