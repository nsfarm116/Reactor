Atom {
	Name = "Batch Change Parameters",
	Category = "Scripts/Comp",
	Author = "Bryan Ray/MuseVFX",
	Version = 4.2,
	Date = {2021, 9, 6},
	Description = [[<p>This script allows you to change parameters for multiple selected tools simultaneously. The selected tools need not be of the same node type. With Batch Change Parameters's GUI, only the Inputs that the tools have in common will be changed.</p>

<p>This script has been updated and works in Fusion v9-17.2.1+ and Resolve v15+.</p>
<hr>
<h2>We Suck Less Thread:</h2>

<p>Batch Parameter Changer — A Rewrite for Fusion 9<br>
<a href="https://www.steakunderwater.com/wesuckless/viewtopic.php?f=6&p=13401#p13147">https://www.steakunderwater.com/wesuckless/viewtopic.php?f=6&p=13401#p13147</a></p>

<hr>
<h2>Change Log:</h2>

<p>v1.0 - 2007-02-22 - 
by SlayerK<br>
	- Initial release</p>

<p>v2.0 - 2018-01-22 - 
by Bryan Ray for MuseVFX<br>
	- Updated for Fusion 9 and UI Manager. Cleaned code and added documentation. Removed orphan and redundant functions. Removed un-implemented math operations code.</p>

<p>v3 - 2019-11-17 - 
By Andrew Hazelden for Reactor<br>
	- Updated to add a copy of "bmd.isin()" function that was renamed to "bcIsIn()" so the script can run in Resolve where the "bmd.scriptlib" file does not exist. Added a TargetID value to the UI Manager window so pressing "Ctrl + W" closes the window instead of the composite.<br>
	- Added error handling for several nil return values from "fuIDlist" and "uIDAttrs.INPID_InputControl" that caused "gusb()" error messages in the Console.<br>
	- Adjusted UI Manager GUI weight values to fix Fusion/Resolve v16 GUI rendering issues.</p>

<p>v4 - 2020-02-05 - 
by Bryan Ray for MuseVFX<br>
	- Improved UI formatting<br>
	- Implemented Operation mode<br>
	- Appended page to control names.</p>

<p>v4.1 - 2020-04-03 - 
by Bryan Ray for MuseVFX<br>
	- Added handling for expressions</p>

<p>v4.2 - 2020-05-21 - 
by Bryan Ray for MuseVFX with input from Movalex<br>
	- Added handling for Loader and Saver Filename inputs<br>
	- Added wildcard text handling for filename inputs<br>
	- use gsub for pattern search.</p>


]],
	Donation = {
		URL = [[https://www.paypal.com/paypalme/BryanRayVFX]],
		Amount = "$3 USD",
	},

	Deploy = {
		"Scripts/Comp/Batch_Change_Parameters.lua",
	},
}
